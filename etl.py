import requests, re

def cutline(table):
    return re.findall("\|-(?:.|\n)+?(?=(?:\|-|\|}))", table.replace("||", "\n|") + "\n")
    
def cutcolumn(line):
    return re.findall("^(?:\||!)(?!-)(?:.|\n)*?(?=(?:\n\||\n!|\n$))",line + "\n", re.M)

def isHeader(column):
    return bool(re.match("^!(?:.|\n)+", column))

def extractMeta(meta):
    return dict(re.findall("(\w+)=\"(.*?)\"", meta))
   
def extractContent(column):
    column += " "
    data = re.findall("^(?:!|\|)(?:((?:\s*\w+=\".*?\"\s*)+)\|)?((?:.|\n)+)", column)
    content = ""
    meta = []
    valid = False
    if data:
        data = data[0]
        valid = True
        if len(data) == 1:
            content = data[0]
        else:
            meta = extractMeta(data[0])
            content = data[1]
    return {
        "content" : content,
        "meta" : meta,
        "valid" : valid
    }

def formatHeaderClock(column):
    vals = extractContent(column)
    time = re.findall("\s*(\d{1,2}(?::|\.)\d{2})\s*.?\s*(\d{2}(?::|\.)\d{2})\s*", vals["content"])
    begin = time[0][0] if time else ""
    end = time[0][1] if time else ""
    valid = bool(time) and vals["valid"]
    return {
        "type" : "header",
        "subtype" : "clock" if valid else "other",
        "meta" : vals['meta'],
        "content" : vals["content"],
        "begin" : begin,
        "end" : end,
        "valid" : valid
    }
    
def formatHeaderRoom(column):
    vals = extractContent(column)
    valid = vals["valid"]
    return {
        "type" : "header",
        "subtype" : "room" if valid else "other",
        "meta" : vals['meta'],
        "content" : vals["content"].replace("<br />", "").replace("<br/>", ""),
        "valid" : valid
    }

def formatColumnClock(column):
    vals = extractContent(column)
    cat = ""
    if 'class' in vals['meta']:
        s = re.findall("(?:^|\s)(hintergrundfarbe\d+)(?:$|\s)", vals['meta']['class'])
        cat = s[0] if s else ""
    if 'colspan' in vals['meta']:
        if int(vals['meta']['colspan']) > 1:
            cat = "all"
    title = ""
    link = ""
    ts = re.findall("'''(.*?)'''", vals['content'])
    if ts:
        title = ts[0]
        title = re.sub("\[\[(?:.*?\|)?(.*?)\]\]", r"\1", title).replace("/", "")
        ls = re.findall("\[\[.*?(?:\|.*?)?\]\]", ts[0])
        link = ls[0] if ls else ""
    valid = vals["valid"] and bool(title)
    return {
        "type" : "column",
        "subtype" : "session" if valid else "other",
        "meta" : vals['meta'],
        "title" : title,
        "link" : link,
        "description" : vals['content'],
        "category" : cat,
        "rowspan" : int(vals['meta']['rowspan']) if 'rowspan' in vals['meta'] else 1,
        "valid" : valid
    }

def formatcolumn(column, first=False):
    if isHeader(column):
        if first:
            return formatHeaderRoom(column)
        else:
            return formatHeaderClock(column)
    return formatColumnClock(column)
   
    
def extracttable(table):
    return [[c for c in cutcolumn(l)] for l in cutline(table)]

def extractRooms(extract):
    return [formatcolumn(c, True) for c in extract[0]]
    
def extractSchedule(extract):
    return [[formatcolumn(c) for c in l] for l in extract[1:]]
    
def prepareRooms(room):
    return {
        "room" : room['content'],
        "rowspan" : 1,
        "session-spread" : None
    }

def initRooms(extrooms):
    return [prepareRooms(room) for room in extrooms[1:-1]]

def addSession(load, sessionID, column, room, time):
    error = False
    if not 'valid' in column:
        error = True
    if not column['valid']:
        error = True
    sessionID = str(sessionID)
    if not error:
        load[sessionID] = column
        load[sessionID]['id'] = sessionID
        load[sessionID]['room'] = room
        load[sessionID]['times'] = [time]
    return {
        "load": load,
        "roommgr": {
            "room" : room,
            "rowspan" : column['rowspan'] if 'rowspan' in column else 1,
            "session-spread" : sessionID
        },
        "error": error
    }
    

def addTimeSession(load, sessionID, time):
    error = True
    sessionID = str(sessionID)
    if sessionID in load :
        error = False
        load[sessionID]['times'].append(time)
    return {
        "load": load,
        "error": error
    }

def doExtract(table):
    extract = extracttable(table)
    return {
        "extrooms" : extractRooms(extract),
        "extschedule" : extractSchedule(extract)
    }

def parseExtracts(extract):
    extrooms = extract['extrooms']
    extschedule = extract['extschedule']
    
    roomsManager = initRooms(extrooms)
    
    sid = 1
    load = {}
    sorder = []
    
    for r in extschedule:
        time = r[0]
        if not 'valid' in time:
            roomsManager = initRooms(extrooms)
            continue
        if not time['valid']:
            roomsManager = initRooms(extrooms)
            continue
        colspan = 1
        i = 0
        for j, room in enumerate(roomsManager):
            sid += 1
            if colspan > 1:
                colspan -= 1
                continue
            if i+1 < len(r):
                if 'colspan' in r[i+1]['meta']:
                    colspan = int(r[i+1]['meta']['colspan'])
            if room['rowspan'] == 1:
                if i+1 < len(r):
                    res = addSession(load, 'session-' + str(sid), r[i+1], room['room'], time)
                    if not res['error']:
                        load = res['load']
                        sorder.append('session-' + str(sid))
                    roomsManager[j] = res['roommgr']
                i += 1
            else:
                res = addTimeSession(load, room['session-spread'], time)
                if not res['error']:
                    load = res['load']
                roomsManager[j]['rowspan'] -= 1
            
    return [load[_] for _ in sorder]
    
def finalize(day, session, prefix=''):
    cat = {
        "hintergrundfarbe3": "Community",
        "hintergrundfarbe7": "Freies Wissen/Freie Projekte",
        "hintergrundfarbe4": "Recht",
        "hintergrundfarbe9": "Technik",
        "hintergrundfarbe6": "Wikimedia-Projeckte",
        "hintergrundfarbe8": "ohne Zuordnung",
        "all": "Jeder"
    }
    start = None
    finish = None
    duration = None
    for time in session['times']:
        if not 'valid' in time:
            continue
        if not time['valid']:
            continue
        b = int(time['begin'].replace(':', '').replace('.', ''))
        e = int(time['end'].replace(':', '').replace('.', ''))
        if not start or b < start :
            start = b
        if not finish or e > finish:
            finish = e
    if start is not None and finish is not None:
        h = int(finish/100) - int(start/100) - (1 if finish%100 - start%100 < 0 else 0)
        m = finish%100 - start%100
        duration = ((str(h) + ' uhr ') if h > 0 else '') + (str(m if m >= 0 else 60+m) + ' min' if m != 0 else '')
    return {
        "show": "{{{show|Simple}}}",
        "id": prefix + session['id'],
        "title": session['title'],
        "timeblock": "tb-" + str(day).lower(),
        "start": str(start)[:len(str(start))-2] + ':' + str(start)[-2:],
        "duration": duration,
        "description" : session['description'],
        "link": session['link'],
        "location": session['room'],
        "theme": cat[session['category']] if session['category'] in cat else ""
    }
    
def etl(text):
    tables = re.findall("{\|(?:.*\n)*?\|}", text)

    days = ['Freitag', 'Samstag', 'Sonntag']
    tables = tables[2:5]
    
    ret = []
    
    for day, table in zip(days, tables):
        ret += [finalize(day, _, day.lower() + '-') for _ in parseExtracts(doExtract(table))]
    
    return ret

def wiki(etl):
    ret = ""
    for e in etl:
        t = "{{\n"
        t += "WikiCon/2019/Session\n"
        keys = list(e.keys())
        keys.sort()
        for key in keys:
            t += "|" + str(key) + '=' + e[key] + '\n' 
        t += "}}\n"
        ret += t
    ret += "{{Wikipedia:WikiCon 2019/Programm/Config}}"
    ret = re.sub("\[\[\/(.*?)\/\]\]", r"[[Wikipedia:WikiCon_2019/Programm/\1|\1]]", ret)
    ret = re.sub("\[\[(\/.*?)\]\]", r"[[Wikipedia:WikiCon_2019/Programm\1]]", ret)
    return ret
        

#Corp du script

response = requests.get("https://de.wikipedia.org/w/api.php?action=query&format=json&prop=revisions&meta=&titles=Wikipedia%3AWikiCon_2019%2FProgramm&rvprop=content&rvlimit=1")

text = ""
for e in response.json()["query"]["pages"]:
    for f in response.json()["query"]["pages"][e]['revisions'] :
        text = f["*"]

f = open("/data/project/germancon-mobile/refactor-schedule/.etl.txt", "w+")
f.write(wiki(etl(text)))
f.close()

